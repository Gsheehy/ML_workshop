from skimage.segmentation import flood_fill
from skimage.filters import median
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import datasets
from scipy.ndimage import binary_erosion, binary_dilation, binary_opening, binary_closing


def load_iris():
    # Loading the iris dataset from sklearn
    iris = datasets.load_iris()
    # Unpacking relevant iris informations
    X = iris['data']
    y = iris['target']
    feature_names = iris['feature_names']
    label_names = iris['target_names']

    # Plotting with seaborn
    data = {'species': label_names[y]}
    i = 0
    for featurename in feature_names:
        data[featurename] = X[:, i]
        i += 1

    iris = pd.DataFrame(data)
    return iris


def smooth_spots(spot):
    # median filter
    spot_ = median(spot)

    # erosion cleanup
    ksize = 7
    xx, yy = np.meshgrid(range(ksize), range(ksize))
    kernel = (xx - ksize // 2)**2 + (yy - ksize // 2)**2 < (ksize / 2)**2
    spot_ = binary_erosion(spot_, kernel)

    return spot_


def spot_count(spot):
    count = 0
    m, n = spot.shape
    for i in range(m):
        for j in range(n):
            if spot[i, j] == 0:
                continue
            count += 1
            spot = flood_fill(spot, (i, j), 0)

    return count


def rotdata(X, angle):
    x_ = X[:, 0] * np.cos(angle) - X[:, 1] * np.sin(angle)
    y_ = X[:, 0] * np.sin(angle) + X[:, 1] * np.cos(angle)
    X[:, 0] = x_
    X[:, 1] = y_
    return X
